<%--
  Created by IntelliJ IDEA.
  User: WuYun
  Date: 2020-10-1
  Time: 上午 08:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录页面</title>
</head>
<body>
<form method="post" action="./loginServlet">
    <table>
        <tr>
            <td>
                用户名:
            </td>
            <td>
                <label>
                    <input type="text" name="user">
                </label>
            </td>
        </tr>
        <tr>
            <td>
                密码:
            </td>
            <td>
                <label>
                    <input type="password" name="password">
                </label>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="登录">
            </td>
        </tr>
    </table>
</form>
</body>
</html>
