package com.demo.servlet;

import com.demo.tableoperator.List_man_table;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;


public class LoginServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user=req.getParameter("user");
        String password=req.getParameter("password");
        List_man_table table = new List_man_table();
        Object[] name = {user,password};
        Map<String, Object> map = table.getMan(name);
        if (map == null) {
            resp.getWriter().println("fail");
            //req.getRequestDispatcher("/loginFail.jsp").forward(req, resp);
        } else {
            req.getSession().setAttribute("user",user);
            resp.getWriter().println("succeed");
            //req.getRequestDispatcher("/loginSucceed.jsp").forward(req, resp);
        }
    }
}
