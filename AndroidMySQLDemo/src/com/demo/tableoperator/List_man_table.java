package com.demo.tableoperator;

import com.demo.DBUtil.DBUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class List_man_table {
    public void addToTable(Object[]obj){
        if(obj.length==3) {
            String sql = "insert into `list_man` value(?,?,?)";
            PreparedStatement preparedStatement = DBUtil.getPrepared(sql);
            try {
                DBUtil.setPreparedStatement(obj, preparedStatement);
                preparedStatement.execute();
                DBUtil.close(preparedStatement);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public void removeFromTable(Object[]objects){
        if(objects.length==1) {
            String sql = "delete from `list_man` where `name`=?";
            PreparedStatement preparedStatement = DBUtil.getPrepared(sql);
            try {
                DBUtil.setPreparedStatement(objects,preparedStatement);
                preparedStatement.execute();
                DBUtil.close(preparedStatement);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public void modifyTable(Object[]objects){
        if(objects.length==2) {
            String sql = "update `list_man` set `name`=?,`password`=? where `id`=?";
            PreparedStatement preparedStatement = DBUtil.getPrepared(sql);
            try {
                DBUtil.setPreparedStatement(objects,preparedStatement);
                preparedStatement.execute();
                DBUtil.close(preparedStatement);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public Map<String,Object> getMan(Object[]objects){
        List<Map<String,Object>>list=null;
        if(objects.length==2){
            String sql = "select `name`,`password` from `list_man` where `name`=? and `password`=?";
            PreparedStatement preparedStatement = DBUtil.getPrepared(sql);
            try {
                DBUtil.setPreparedStatement(objects,preparedStatement);
                if(preparedStatement!=null) {
                    ResultSet resultSet = preparedStatement.executeQuery();
                    list = DBUtil.getResult(resultSet);
                    DBUtil.close(preparedStatement, resultSet);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else if(objects.length==1){
            String sql = "select `name`,`password` from `list_man` where `name`=?";
            PreparedStatement preparedStatement = DBUtil.getPrepared(sql);
            try {
                DBUtil.setPreparedStatement(objects,preparedStatement);
                ResultSet resultSet=preparedStatement.executeQuery();
                list=DBUtil.getResult(resultSet);

                DBUtil.close(preparedStatement,resultSet);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(list!=null&&list.size()==1)
            return list.get(0);
        return null;
    }
}
