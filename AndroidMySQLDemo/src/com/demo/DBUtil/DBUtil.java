package com.demo.DBUtil;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//用于数据库的工具类
public class DBUtil {
    private static final String driver="com.mysql.cj.jdbc.Driver";
    private static final String user="root";
    private static final String password="root";
    private static final String databaseName="demo_man";
    private static final String ip="127.0.0.1";
    private static final String port="3306";
    //private static final String tableName="list_man";

    private static final String url="jdbc:mysql:"+
            "//"+ip+":"+port+
            "/"+databaseName
            +"?useSSL=false&serverTimezone=UTC";

    private static Connection conn=null;
    //获取getPreparedStatement对象
    public static PreparedStatement getPrepared(String sql){
        PreparedStatement preparedStatement=null;
        try {
            if(conn==null){
                Class.forName(driver);

                conn= DriverManager.getConnection(url, user, password);
            }
            preparedStatement=conn.prepareStatement(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return preparedStatement;
    }
    //通过Result对象获取内容
    public static List<Map<String, Object>> getResult(ResultSet resultSet) {
        List<Map<String,Object>> list= null;
        try {
            if(resultSet!=null) {
                list=new ArrayList<>();
                ResultSetMetaData meta = resultSet.getMetaData();//可以得到结果集的表结构
                String[] columns = new String[meta.getColumnCount()];
                for (int i = 0; i < columns.length; i++) {
                    columns[i] = meta.getColumnName(i + 1);
                }
                while (resultSet.next()) {
                    Map<String, Object> map = new HashMap<>();
                    for (String column : columns) {
                        map.put(column, resultSet.getObject(column));
                    }
                    list.add(map);
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return list;
    }
    //预定义语句赋值
    public static void setPreparedStatement(Object[] obj, PreparedStatement prep) throws SQLException {
        if(prep!=null) {
            for (int i = 0; i < obj.length; i++) {
                prep.setObject(i + 1, obj[i]);
            }
        }
    }
    //关闭数据库流
    public static void close(PreparedStatement prep,ResultSet rs){
        try {
            if (rs!=null)rs.close();
            if (prep!=null)prep.close();
            if (conn!=null){
                conn.close();
                conn=null;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    //关闭数据库流
    public static void close(PreparedStatement prep){
        try {
            if (prep!=null)prep.close();
            if (conn!=null){
                conn.close();
                conn=null;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}

